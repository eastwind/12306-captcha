# 12306-captcha

#### 介绍
12306验证码识别服务，基于springboot webscoket开发。

https://12306.jiedanba.cn 此网站的验证码服务将不再提供，需要使用的可以自己部署

#### 部署环境
后台项目可随意部署，12306YZM 插件必须在windows下运行，并且需要国产浏览器，360极速 和360安全浏览器都可以

#### 安装教程

1.先启动好后台服务，并且在浏览器上安装12306YZM 插件。
2.在浏览器运行GetSig.js即可了。



#### 图片介绍

![项目结构](https://images.gitee.com/uploads/images/2019/0603/224200_a8c5356a_955082.png)

![浏览器运行界面](https://images.gitee.com/uploads/images/2019/0603/223807_86e7ae1f_955082.png)


![首页](https://images.gitee.com/uploads/images/2019/0603/223957_182c2cda_955082.png)



